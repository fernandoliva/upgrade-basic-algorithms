// Iteración #1: Variables
// 1.1 Crea una variable llamada myFavoriteHero, asigna el valor Hulk a ella.
// 1.2 Crea una variable llamada x, asigna el valor 50 a ella.
// 1.3 Crea una variable llamada 'h' con el valor 5 y otra 'y' con el valor 10. 
// 1.4 Crea una otra variable 'z' y asignale el valor de 'h' + 'y'.
/*--------------------------------------------------------------------------------------------------*/

// 1.1
let myFavoriteHero = 'Hulk';

// 1.2
let x = 50;

// 1.3
let h = 5;

// 1.4
let y = 10;

console.log('1.1: '+ myFavoriteHero +'\n 1.2: '+ x +'\n 1.3: '+ h +'\n 1.4: '+y);