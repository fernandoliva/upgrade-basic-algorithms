// Vamos a crear un garaje de coches, tenemos mucha pasta asi que necesitamos gestionarlo.
// Para ello vamos a crear una array coche1 que va a tener los valores de marca=audi, modelo=A8, itv=true, combustible=gasolina. 
// Despues creamos los siguientes.
// coche2 --> marca=Ferrari, modelo=enzo, itv=false, combustible=gasolina
// coche3 --> marca= skoda, modelo= tanque de combate, itv=false, combustible= diesel
// avion1 --> marca=airbus, modelo=A300, combustible= queroseno, capacidad=200
// barco1 --> marca=bombardier, modelo=superyate, combustible=diesel
// barco2 --> marca= yamaha, modelo=A350, combustible= gasolina

// Una vez tengamos definidos nuestros vehiculos vamos a crear una variable garaje que va a estar compuesta por:

// coches--> va a ser un array con los coches 1,2 y 3.
// avion --> va a ser un array con los aviones 
// barco --> va a ser un array con los barcos (1,2)

// imprimir por consola la variable garajes
// imprimir por consola todos los coches dentro de la variable garajes
// imprimir por consola todos los aviones  dentro de la variable garajes
// imprimir por consola todos los barcos dentro de la variable garajes

const coche1 = {
    marca: "audi",
    modelo: "A8",
    itv: true,
    combustible: "gasolina"
}

const coche2 = {
    marca: "ferrari",
    modelo: "enzo",
    itv: false,
    combustible: "gasolina"
}

const coche3 = {
    marca: "skoda",
    modelo: "tanque de combate",
    itv: false,
    combustible: "diesel"
}

const avion = {
    marca: "airbus",
    modelo: "A300",
    combustible: "queroseno",
    capacidad: 200
}

const barco1 = {
    marca: "bombardier",
    modelo: "superyate",
    combustible: "diesel"
}

const barco2 = {
    marca: "yamaha",
    modelo: "A350",
    combustible: "gasolina"
}

//Pasamos array a objeto
const garaje = {
    coches : [coche1,coche2,coche3],
    aviones : [avion],
    barcos : [barco1,barco2]
}

//console.log(garaje);

// console.log(garaje.coches);
// console.log(garaje.aviones);
// console.log(garaje.barcos);

// basandonos en el ejercicio del garaje. se solicita mostrar por consola:
// Recorrer todos los coches e imprimir la marca y el modelo.
// for (value of garaje.coches){
//     console.log('Marca -> '+value.marca);
//     console.log('Modelo -> '+value.modelo);
//     console.log('');
// }
// Recorrer todos los vehiculos e imprimir su marca.
// for (value of garaje.coches){
//     console.log(value.marca);
// }
// console.log('');

// for (value of garaje.aviones){
//     console.log(value.marca);
// }

// console.log('');

// for (value of garaje.barcos){
//     console.log(value.marca);
// }

// Recorrer todos los coches e imprimir los que no tengan la itv pasada
// for (value of garaje.coches){
//     if (value.itv == true){
//         console.log(`Coches que tienen pasada la itv: ${value.marca}`);
//     }
// }

// Recorrer los barcos e imprimir todos en los que el modelo sea 'superyate'
// for (value of garaje.barcos){
//     if (value.modelo === 'superyate'){
//         console.log(`Barcos que son superyates: ${value.marca}`);
//     }
// }

// Recorrer todo el garaje e imprimir los vehiculos que utilicen gasolina.

// for (value of garaje.coches){
//     if (value.combustible === 'gasolina'){
//         console.log(`Coches que utilizan gasolina: ${value.marca}`);
//     }
// }