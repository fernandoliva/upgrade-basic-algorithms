//Iteración #4: Arrays

// 1.1 Consigue el valor "HULK" del array de avengers y muestralo por consola.
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

// 1.2 Cambia el primer elemento de avengers a "IRONMAN"
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

// 1.3 Console numero de elementos en el array usando la propiedad correcta de Array.
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

// 1.4 Añade 2 elementos al array: "Morty" y "Summer". 
// Muestra en consola el último personaje del array
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];

// 1.5 Elimina el último elemento del array y muestra el primero y el último por consola.
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];

// 1.6 Elimina el segundo elemento del array y muestra el array por consola.
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
/*--------------------------------------------------------------------------------------------------*/

//1.1
// const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
// console.log(avengers.indexOf('HULK'));

//1.2
// avengers.shift();
// avengers.unshift("IRON MAN");
// console.log(avengers);

//1.3
// console.log(avengers.length);

//1.4
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
// rickAndMortyCharacters.push("Morty");
// rickAndMortyCharacters.push("Summer");
// console.log(rickAndMortyCharacters.reverse()[0]);


//1.5
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
// rickAndMortyCharacters.pop();
// console.log('Primero: ' + rickAndMortyCharacters[0] + '\n Segundo: ' + rickAndMortyCharacters.reverse()[0]);

//1.6
// const rickAndMortyCharacters = ["Rick", "Beth", "Jerry", "Morty", "Summer", "Lapiz Lopez"];
// rickAndMortyCharacters.splice(1,1);
// console.log(rickAndMortyCharacters);